const TaskStatus = {
  DOING: "Doing",
  DONE: "Done",
  REMOVE: "REMOVE"
}

export default TaskStatus;