import React, { useState, useEffect } from 'react';
import { List, Card } from 'antd';

const UserAlbum = (props) => {
  const [UserAlbum, setUserAlbum] = useState([]);

  useEffect(() => {
    fetchUserAlbum();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchUserAlbum = () => {
    (async () => {
      try {
        let response = await fetch(`https://jsonplaceholder.typicode.com/photos?albumId=${props.match.params.albumId}`);
        let data = await response.json();
        setUserAlbum(data);
      } catch(err) {
        console.log(err);
      }
    })();
  }
  return (
    <div>
      <List
        grid={{
          gutter: 16,
          xs: 1,
          sm: 2,
          md: 4,
          lg: 4,
          xl: 6,
          xxl: 3,
        }}
        loading={UserAlbum.length === 0}
        dataSource={UserAlbum}
        renderItem={item => (
          <List.Item>
            <Card title={item.title}>
              <img src={item.thumbnailUrl} alt={item.title}></img>
            </Card>
          </List.Item>
        )}
      />
    </div>
  );
}

export default UserAlbum;