import React from 'react';
import { Menu } from 'antd';

const NavBar = (props) => {
  return (
    <Menu
      theme="dark"
      mode="horizontal"
      defaultSelectedKeys={props.location.pathname === "/" || 
      props.location.pathname === "/users" ? ['1'] : ['-1']}
      style={{ lineHeight: '64px', paddingLeft: '40px' }}
    >
      <Menu.Item key="1" onClick={() => {
        window.location = "/users";
      }}>Users</Menu.Item>
    </Menu>
  );
}

export default NavBar;