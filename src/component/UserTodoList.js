import React, { useState, useEffect } from 'react';
import { Row, Col, Typography, Table, Tag, Button, Select } from 'antd';
import TaskStatus from '../constants/TaskStatus';
import { useDispatch, useSelector } from 'react-redux';
import { setTasks, updateTask, filterTasks } from '../actions/taskAction';

const { Column } = Table;
const { Option } = Select;

const UserTaskList = (props) => {
  const dispatch = useDispatch();
  const taskState = useSelector(state => state.taskReducer);

  const [taskList, setTaskList] = useState([]);
  
  useEffect(() => {
    fetchUserTasks();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchUserTasks = () => {
    (async () => {
      try {
        let response = await fetch(`https://jsonplaceholder.typicode.com/todos?userId=${props.match.params.userId}`);
        let data = await response.json();
        setTaskList(data);
        dispatch(setTasks(data));
      } catch(err) {
        console.log(err);
      }
    })();
  }

  const renderTitle = (data) => {
    return (
      <Typography.Text delete={data.completed}>{data.title}</Typography.Text>
    );
  }

  const renderStatus = (data) => {
    return (
      <Tag 
      color={data.completed ? "green" : "blue"}>
        {data.completed ? "Done" : "Doing"}
      </Tag>
    );
  }

  const renderButtons = (data) => {
    if (data.completed) {
      return (
        <span>
          <Button type="primary" onClick={() => dispatch(updateTask(data, TaskStatus.DOING))}>Reopen</Button>
          <Button type="danger" onClick={() => dispatch(updateTask(data, TaskStatus.REMOVE))}>Remove</Button>
        </span>
      );
    }
    return (
      <Button type="primary" onClick={() => dispatch(updateTask(data, TaskStatus.DONE))}>Done</Button>
    );
  }

  const filter = (data) => {
    let filteredData = data;

    filteredData = filteredData.filter(taskTypeFilter);

    return filteredData;
  }

  const taskTypeFilter = (task) => {
    let value = taskState.filterValue;
    if (value === "ALL") {
      return true;
    } else {
      let status = value === TaskStatus.DONE ? true : false;
      return task["completed"] === status;
    }
  }

  return (
    <div>
      <Row type="flex" justify="center">
        <Col span={16} className="content padding-20">
          <Typography.Title className="text-align-center">
            Task list
          </Typography.Title>
          <Select defaultValue="All" style={{ width: 120 }} onChange={(value) => {
              dispatch(setTasks(taskList));
              dispatch(filterTasks(value));
            }}>
            <Option value="ALL">All</Option>
            <Option value={TaskStatus.DOING}>Doing</Option>
            <Option value={TaskStatus.DONE}>Done</Option>
          </Select>
          <Table 
            dataSource={filter(taskState.tasks)} 
            rowKey="id"
            loading={taskList.length === 0}
          >
            <Column title="Task id" dataIndex="id" />
            <Column title="Title" render={data => renderTitle(data)} />
            <Column title="Status" render={data => renderStatus(data)} />
            <Column title="Action" render={data => renderButtons(data)} />
          </Table>
        </Col>
      </Row>
    </div>
  );
}

export default UserTaskList;