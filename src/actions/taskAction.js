import { UPDATE_TASK, SET_TASKS, FILTER_TASKS } from "../constants/actionTypes";

export const updateTask = (task, taskStatus) => {
  return {
    type: UPDATE_TASK,
    payload: {
      task,
      taskStatus
    }
  };
}

export const setTasks = (tasks) => {
  return {
    type: SET_TASKS,
    payload: tasks
  }
}

export const filterTasks = (value) => {
  return {
    type: FILTER_TASKS,
    payload: value
  }
}
