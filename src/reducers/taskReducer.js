import { UPDATE_TASK, SET_TASKS, FILTER_TASKS } from "../constants/actionTypes";
import TaskStatus from "../constants/TaskStatus";

const initialState = {
  tasks: [],
  filterValue: "ALL"
}

const taskReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_TASKS:
      return Object.assign({}, state, {
        tasks: action.payload
      })
    case UPDATE_TASK:
      let tmpTaskList = [...state.tasks];
      let taskIndex = tmpTaskList.findIndex((item) => {
        return item.id === action.payload["task"].id;
      });
      switch (action.payload["taskStatus"]) {
        case TaskStatus.DONE:
          tmpTaskList[taskIndex].completed = true;
          break;
        case TaskStatus.DOING:
          tmpTaskList[taskIndex].completed = false;
          break;
        case TaskStatus.REMOVE:
          tmpTaskList.splice(taskIndex, 1);
          break;
        default:
          break;
      }
      return Object.assign({}, state, {
        tasks: tmpTaskList
      })
    case FILTER_TASKS:
      return Object.assign({}, state, {
        filterValue: action.payload
      });
    default:
      return state;
  }
}

export default taskReducer;